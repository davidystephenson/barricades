﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;


public class Setup : MonoBehaviour {

	public Camera mainCamera;
	public GameObject mainMount;
	public GameObject player;

	// Use this for initialization
	void Start () {

		Vector2 halfView = new Vector2(15, 10);

		Physics2D.gravity = Vector2.zero;

		player = MakePlayer();

		mainMount = MakeMount();
		mainCamera = MakeCamera(mount: mainMount);
		mainCamera.transform.parent = player.transform;

		MakeWall(new Vector3(5, 5), 10, 5, 0);

		GameObject darkness = MakeDarkness();

		GameObject lightMask = Make(name: "Light Mask", parent: player);
		lightMask.transform.localPosition = new Vector3(0, 4, 0);
		lightMask.transform.localScale = new Vector3(halfView.x, halfView.y, 1);
		Vector2[] verts = BoxVectors(size: 0.5f, x: 0.5f, y: 0.5f);
		Sprite maskSprite = Sprite.Create(Texture2D.whiteTexture,new Rect(0, 0, 1, 1),new Vector2(0.5f, 0.5f),1);
		maskSprite.OverrideGeometry(verts, new ushort[] { 0, 1, 3, 1, 2, 3 });
		SpriteMask curtainSpriteMask = lightMask.AddComponent<SpriteMask>();
		curtainSpriteMask.sprite = maskSprite;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static GameObject Make (
		string name,
		Vector3? position = null,
		Vector3? scale = null,
		GameObject parent = null
	) {
		GameObject gameObject = new GameObject();
		gameObject.name = name;
		gameObject.transform.position = position ?? Vector3.zero;

		if (parent)
		{
			gameObject.transform.parent = parent.transform;
		}

		gameObject.transform.localScale = scale ?? Vector3.one;

		return gameObject;
	}

	GameObject MakeDarkness () {
		GameObject darkness = Make(name: "Darkness", scale: new Vector3(500, 500, 1));

		SpriteRenderer renderer = darkness.AddComponent<SpriteRenderer>();
		renderer.sprite = MakeSquare(name: "Darkness");
		renderer.color = Color.black;
		renderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
		renderer.sortingLayerName = "Curtain";

		return darkness;
	}

	GameObject MakePlayer() {
		int count = 5;
		float slice = 360.0f / count;
		int FORWARD = 90;
		Vector2 spine = 0.5f * Vector2.one;

		Vector2[] vertices = new Vector2[count];
		for (int i = 0; i < count; i++) {
			float rotation = i * slice;
			float angleDegrees = (FORWARD + rotation);
			float angleRadians = angleDegrees * Mathf.PI / 180;

			float xDistance = 0.5f * Mathf.Cos(angleRadians);
			float yDistance = 0.5f * Mathf.Sin(angleRadians);

			Vector2 x = xDistance * Vector2.right;
			Vector2 y = yDistance * Vector2.up;

			vertices[i] = spine + x + y;
		}

		Triangulator triangulator = new Triangulator(vertices);
		ushort[] indices = triangulator
			.Triangulate()
			.Select(x => (ushort)x)
			.ToArray();

		Sprite polygon = MakeSquare(name: "Polygon");
		polygon.OverrideGeometry(vertices, indices);

		GameObject player = Make(
			name: "Player",
			position: Vector2.zero,
			scale: 3 * Vector3.one
		);
	
		SpriteRenderer renderer = player.AddComponent<SpriteRenderer>();
		renderer.color = Color.blue;
		renderer.sortingLayerName = "People";
		renderer.sprite = polygon;
		// This must come after the sprite render is added
		player.AddComponent<PolygonCollider2D>();

		Rigidbody2D body = player.AddComponent<Rigidbody2D>();
		body.drag = 5;
		body.angularDrag = 2;

		player.AddComponent<InputController>();
		player.AddComponent<PlayerInput>();
		player.AddComponent<Move>();

		return player;
	}

	GameObject MakeWall(
		Vector3 position,
		float halfHeight,
		float halfWidth,
		float rotation
	) {
		GameObject wall = Make(name: "Wall", position: position);
		wall.transform.localScale = new Vector3(2 * halfWidth, 2 * halfHeight, 1);
		wall.transform.eulerAngles = new Vector3(0, 0, rotation);

		SpriteRenderer renderer = wall.AddComponent<SpriteRenderer>();
		renderer.color = Color.green;
		renderer.sortingLayerName = "Walls";
		renderer.sprite = MakeSquare(name: "Wall");

		PolygonCollider2D collider = wall.AddComponent<PolygonCollider2D>();
		collider.CreatePrimitive(sides: 4);
		collider.points = BoxVectors(size: 0.5f);

		return wall;
	}

	float[][] BoxPoints (float size, float x = 0, float y = 0) {
		float[] upperLeft = new float[] { -size + x, -size + y };
		float[] bottomLeft = new float[] { -size + x, size + y };
		float[] bottomRight = new float[] { size + x, size + y };
		float[] upperRight = new float[] { size + x, -size + y };

		return new float[][] { upperLeft, bottomLeft, bottomRight, upperRight };
	}

	Vector2[] BoxVectors(float size, float x = 0, float y = 0) {
		float[][] points = BoxPoints(size, x, y); 

		Vector2[] vectors = points
			.Select(point => new Vector2(point[0], point[1]))
			.ToArray();
		
		return vectors;
	}

	GameObject MakeMount (string name = "MainMount") {
		GameObject mount = Make(name: name);
		mount.transform.position = 3 * Vector3.back;

		return mount;
	}

	Camera MakeCamera (GameObject mount) {
		Camera camera = mount.AddComponent<Camera>();
		camera.orthographic = true;
		camera.orthographicSize = 40f;
		camera.backgroundColor = Color.white;
		
		return camera;
	}

	Sprite MakeSquare (string name) {
		Sprite sprite = Sprite.Create(
			Texture2D.whiteTexture, // content
			new Rect(0, 0, 1, 1), // bounding box
			new Vector2(0.5f, 0.5f), // rotation point
			1 // scale (pixels per world distance)
		);
		sprite.name = name;

		return sprite;
	}
}