using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    private InputController inputController;
    
	// Use this for initialization
	void Start () {
		inputController = GetComponent<InputController>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 direction = Vector2.zero;

        float rightAngle = gameObject.transform.eulerAngles.z * Mathf.Deg2Rad;
        Vector2 right = new Vector2(Mathf.Cos(rightAngle), Mathf.Sin(rightAngle));
        if (inputController.right) direction += right;
        if (inputController.left) direction -= right;
        
        float forwardAngle = rightAngle + 0.5f * Mathf.PI;
        Vector2 forward = new Vector2(Mathf.Cos(forwardAngle), Mathf.Sin(forwardAngle));
        if (inputController.up) direction += forward;
        if (inputController.down) direction -= forward;
        
        float spin = 0;
        if (inputController.turnLeft) spin += 1;
        if (inputController.turnRight) spin -= 1;

        Force(direction, spin);
    }

    void Force (Vector2 direction, float spin) {
        int ACCELERATION = 50;
        int TORQUE = 20;
        
        Rigidbody2D rigidBody2D = GetComponent<Rigidbody2D>();

        direction.Normalize();
        rigidBody2D.AddForce(ACCELERATION * direction);

        rigidBody2D.AddTorque(TORQUE * spin);
    }
}