using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
  public bool up = false;
  public bool down = false;
  public bool left = false;
  public bool right = false;
  public bool turnLeft = false;
  public bool turnRight = false;
  public bool attack = false;
}