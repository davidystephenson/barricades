using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

  private InputController inputController;

	// Use this for initialization
	void Start () {
    inputController = GetComponent<InputController>();
	}
	
	// Update is called once per frame
	void Update () {
    inputController.up = isKeyDown(KeyCode.W, inputController.up);
    inputController.down = isKeyDown(KeyCode.S, inputController.down);
    inputController.left = isKeyDown(KeyCode.A, inputController.left);
    inputController.right = isKeyDown(KeyCode.D, inputController.right);
    inputController.turnLeft = isKeyDown(KeyCode.J, inputController.turnLeft);
    inputController.turnRight = isKeyDown(KeyCode.K, inputController.turnRight);
    inputController.attack = isKeyDown(KeyCode.Space, inputController.attack);
	}

  bool isKeyDown (KeyCode keyCode, bool current) {
    if (Input.GetKeyDown(keyCode)) { 
      return true;
    }

    if (Input.GetKeyUp(keyCode)) {
      return false;
    }

    return current;
  }
}